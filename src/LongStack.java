import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.stream.Collectors;

public class LongStack implements Cloneable {

   public static void main (String[] argum) {
      LongStack stack = new LongStack();
      stack.push(100);
      stack.push(200);
      stack.push(300);
      System.out.println(stack.toString());

      long result = LongStack.interpret("2 3 11 + 5 - *");
      System.out.println(String.format("Result should be 18, was: %s", result));

      // Interpret error case a
      try {
         LongStack.interpret("");
      } catch (Exception e) {
         System.out.println(e.getMessage());
      }

      // Interpret error case a
      try {
         LongStack.interpret(" \n \t");
      } catch (Exception e) {
         System.out.println(e.getMessage());
      }

      // Interpret error case b
      try {
         LongStack.interpret("5 2 - 7 6");
      } catch (Exception e) {
         System.out.println(e.getMessage());
      }

      // Interpret error case c
      try {
         LongStack.interpret("6 7 xxx +");
      } catch (Exception e) {
         System.out.println(e.getMessage());
      }

      // Interpret error case c
      try {
         LongStack.interpret("6 7 %");
      } catch (Exception e) {
         System.out.println(e.getMessage());
      }

      // Interpret error case d
      try {
         LongStack.interpret("3 4 + - 5");
      } catch (Exception e) {
         System.out.println(e.getMessage());
      }
   }

   private ArrayList<Long> stack;

   LongStack() {
      stack = new ArrayList<>();
   }

   @Override
   @SuppressWarnings("unchecked")
   public Object clone() throws CloneNotSupportedException {
      LongStack clone = (LongStack)super.clone();
      clone.stack = (ArrayList<Long>)this.stack.clone();
      return clone;
   }

   public boolean stEmpty() {
      return stack.isEmpty();
   }

   public void push (long a) {
      stack.add(a);
   }

   public long pop() {
      int lastIndex = stack.size() - 1;
      if (lastIndex < 0) {
         throw new IndexOutOfBoundsException("Stack is empty");
      }
      return stack.remove(lastIndex);
   }

   public void op(String s) {
      if (stack.size() < 2) {
         // Interpret error case d - not enough numbers
         throw new UnsupportedOperationException(
                 String.format("Operations require stack size greater or equal than 2 (stack size: %s, operator: \"%s\")",
                         stack.size(), s));
      }

      if (s == null || s.length() != 1) {
         // Interpret error case c (partially) - not allowed symbol
         throw new UnsupportedOperationException(
                 String.format("Invalid operator value \"%s\"", s));
      }

      long operand1 = pop();
      long operand2 = pop();

      long result;
      switch (s.charAt(0)) {
         case '+':
            result = operand2 + operand1;
            break;
         case '-':
            result = operand2 - operand1;
            break;
         case '*':
            result = operand2 * operand1;
            break;
         case '/':
            result = operand2 / operand1;
            break;
         default:
            // Interpret error case c - not allowed symbol
            throw new UnsupportedOperationException(String.format("Invalid operator value \"%s\"", s));
      }
      push(result);
   }
  
   public long tos() {
      int lastIndex = stack.size() - 1;
      if (lastIndex < 0) {
         throw new IndexOutOfBoundsException("Stack is empty");
      }
      return stack.get(lastIndex);
   }

   @Override
   public boolean equals(Object o) {
      if (o == this) {
         return true;
      }
      if (o instanceof LongStack) {
         return stack.equals(((LongStack) o).stack);
      }
      return false;
   }

   @Override
   public String toString() {
      // return stack.toString();
      return stack.stream().map(String::valueOf).collect(Collectors.joining(", "));
   }

   public static long interpret(String pol) {
      if (pol == null) {
         // Interpret error case a - missing expression
         throw new IllegalArgumentException("Interpret expression is null");
      }

      LongStack stack = new LongStack();
      StringTokenizer st = new StringTokenizer(pol);
      if (!st.hasMoreTokens()) {
         // Interpret error case a - missing expression
         throw new IllegalArgumentException(
                 String.format("Interpret expression is empty or contains only whitespaces (expression: \"%s\")", pol));
      }

      do {
         String token = st.nextToken();
         try {
            stack.push(Long.parseLong(token));
         } catch (NumberFormatException exception) {
            try {
               stack.op(token);
            } catch (RuntimeException opException) {
               // Interpret error cases c, d - not allowed symbol or not enough numbers
               throw new RuntimeException(
                       String.format("Unable to evaluate interpret expression \"%s\", operation \"%s\" failed with error: \"%s\"",
                               pol, token, opException.getMessage()));
            }
         }
      } while (st.hasMoreTokens());

      long result = stack.pop();
      if (stack.stEmpty()) {
         return result;
      }
      // Interpret error case b - excess of numbers
      throw new RuntimeException(
              String.format("Interpret expression \"%s\" has excess number arguments (stack: %s, %s)",
                      pol, stack, result));
   }
}
